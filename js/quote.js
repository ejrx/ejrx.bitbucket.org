var quotes = [
    "live by the sword, die by the sword",
    "he who fights and runs away..."
]

window.onload = function() {
    var quoteNode = document.getElementById('site-quote');
    if (quoteNode) {
        quoteNode.textContent = quotes[Math.floor(Math.random()*quotes.length)]
        quoteNode.style.fontStyle = "italic";
    } else {
        quoteNode.textContent = "not found";
    }
}
