var getJSON = function(url) {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', url, true);
        xhr.responseType = 'text';
        xhr.onload = function() {
            var status = xhr.status;
            if (status == 200) {
                resolve(JSON.parse(xhr.responseText));
            } else {
                reject(status);
            }
        };
        xhr.send();
    });
};

var filterPosts = function(posts, queryString) {
    if (queryString === "") {
        return [];
    } else {
        var filtF = function(post) {
            return filterFunc(post, queryString);
        }
        return posts.filter(filtF);
    }
};

var filterFunc = function(post, query) {
    var q = query.toLowerCase();
    if (post.summary.toLowerCase().indexOf(q) > -1) {
        return true;
    } else if (post.title.toLowerCase().indexOf(q) > -1) {
        return true;
    } else if (post.category === q) {
        return true;
    } else {
        return false;
    }
};

window.onload = function() {
    var input = document.getElementById('search-box');
    var resultsList = document.getElementById('search-results-list')

    var search = function(queryStr) {
        if (queryStr != null) {
            getJSON('/search.json').then(function(data) {
                posts = filterPosts(data, queryStr)
                if (posts.length === 0) {
                    showNoResults(queryStr);
                } else {
                    showSearchResults(posts);
                }
            });
        }
    };

    var showNoResults = function(queryStr) {
        var innerText = 'Unable to find posts for "<em>' +
            queryStr + '</em>"';
        if (queryStr === "") {
            innerText = "";
        }
        var li = document.createElement('li');
        li.className = "post-entry";
        li.innerHTML = innerText;
        resultsList.appendChild(li);
    }

    var showSearchResults = function(posts) {
        posts.forEach(function(p) {
            var li = document.createElement('li');
            li.innerHTML = '<a class="post-link" href="' + p.url + '">' + p.title + '</a>';
            resultsList.appendChild(li);
        });
    }

    input.onkeyup = function(e) {
        if (e) {
            resultsList.innerHTML = "";
            if (input.value != "") {
                search(input.value);
            }
        }
    };
};
